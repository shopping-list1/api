from os import getenv

import awsgi

from shopping.api import create_api

APP = create_api(
    items_table_name=getenv("ITEMS_TABLE_NAME"),
    cors_origins=getenv("CORS_ORIGINS").split(",")
)


def handle_apigw(event, context):
    return awsgi.response(APP, event, context)  # pragma: no cover
