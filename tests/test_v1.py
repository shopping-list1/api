"""
For Simple APIs, I prefer to use end-to-end acceptance tests.
They can get you the best coverage, and aren't difficult to configure for small APIs.

For the purpose of this guide, we're not covering any error cases.
"""

from datetime import datetime

from moto import mock_dynamodb2
from pytest import fixture
from falcon.testing import TestClient as FalconClient
from freezegun import freeze_time

from shopping.item import Item
from shopping.api import create_api


@fixture(name="client")
def client_fixture() -> FalconClient:
    """
    In order to perform our end-to-end tests, we're going to build a test client that will simulate HTTP
    calls on our API.  Thankfully, the falcon library can do this for us.

    Instead of going through the pain to set up a local Dynamo DB server, or mocking an Item DAO,
    we're going to use the moto library to simulate the most basic Dynamo DB functionality.
    This way, we get the best coverage with the least configuration.
    """
    with mock_dynamodb2():
        app = create_api(items_table_name="items", cors_origins=[])
        Item.create_table(read_capacity_units=1, write_capacity_units=1)
        yield FalconClient(app)


def test_list(client: FalconClient):
    """
    List items for user1, of which there are 2
    """
    Item(user_id="user1", item_id="item1", created=datetime(2020, 5, 3), name="Basmati Rice").save()
    Item(user_id="user1", item_id="item2", created=datetime(2020, 5, 4), name="Hamburger Buns").save()

    r = client.simulate_get("/items/v1")
    assert 200 == r.status_code
    assert [
        dict(id="item1", created="2020-05-03T00:00:00+00:00", name="Basmati Rice"),
        dict(id="item2", created="2020-05-04T00:00:00+00:00", name="Hamburger Buns")
    ] == r.json


@freeze_time("2020-05-04T00:00:00+00:00")
def test_add(client: FalconClient):
    """
    Add a new item, and ensure it's been added correctly.
    # We're going to mock the current time with the freezegun library, to ensure our tests are repeatable and precise.
    """
    r = client.simulate_post("/items/v1", json=dict(name="Hamburger Patties"))
    assert 200 == r.status_code

    items = list(Item.scan())
    assert 1 == len(items)
    assert "user1" == items[0].user_id
    assert bool(items[0].item_id)
    assert datetime.fromisoformat("2020-05-04T00:00:00+00:00") == items[0].created
    assert "Hamburger Patties" == items[0].name


def test_delete(client: FalconClient):
    """
    Delete an item and ensure it's deleted from the database
    """
    Item(user_id="user1", item_id="item1", name="Basmati Rice").save()

    r = client.simulate_delete("/items/v1/item1")
    assert 200 == r.status_code

    assert [] == list(Item.scan())
