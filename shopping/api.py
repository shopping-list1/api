"""
The falcon API handler for the Shopping List API.

All error-handling is omitted for the purpose of this guide.
"""

from typing import List


from falcon import Request, Response, API
from falcon_cors import CORS

from .item import Item


def get_user_id(request: Request) -> str:
    """
    TODO get user_id from authorization

    For now, we will assume the user_id is "user1"
    """
    return "user1"


class ItemsResource(object):

    @staticmethod
    def on_get(req: Request, resp: Response):
        items = list(Item.query(hash_key=get_user_id(req)))

        resp.media = [
            dict(id=item.item_id, created=item.created.isoformat(), name=item.name)
            for item in items
        ]

    @staticmethod
    def on_post(req: Request, _resp: Response):
        name = req.media["name"]

        item = Item(user_id=get_user_id(req), name=name)
        item.save()


class SingleItemResource(object):

    @staticmethod
    def on_delete(req: Request, _resp: Response, item_id: str):
        item = Item.get(hash_key=get_user_id(req), range_key=item_id)
        item.delete()


def create_api(items_table_name: str, cors_origins: List[str]) -> API:
    Item.Meta.table_name = items_table_name

    cors = CORS(allow_origins_list=cors_origins)

    app = API(middleware=[cors.middleware])
    app.add_route("/items/v1", ItemsResource())
    app.add_route("/items/v1/{item_id}", SingleItemResource())

    return app
