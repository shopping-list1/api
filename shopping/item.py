from datetime import datetime
from uuid import uuid4

from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, UTCDateTimeAttribute


class Item(Model):
    """
    Our Item model uses pynamodb to act as its own DAO.

    We want to give items a unique id, and keep them separated from other users.
    To accomplish this, we take advantage of Dynamo hash and range keys, for optimal performance and cost.

    The 'user_id' hash key reduces the scope to a single user, and the 'item_id' range key uniquely identifies an item.

    Using this compound key structure, we don't need any indices to efficiently list and get items for a user.
    """

    class Meta:
        table_name = ""  # replaced on app start

    # Keys
    user_id = UnicodeAttribute(hash_key=True)
    item_id = UnicodeAttribute(range_key=True, default=lambda: str(uuid4()))

    # Data
    created = UTCDateTimeAttribute(null=False, default=lambda: datetime.utcnow())
    name = UnicodeAttribute(null=False)
